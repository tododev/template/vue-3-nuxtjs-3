# Deploy nuxt 3 with tailwind css using gitlab ci/cd

This readme outlines how to set up and deploy a **nuxt 3** application with **tailwind css** to a **vps** using **gitlab ci/cd**. it includes instructions on setting up ssh keys, configuring gitlab ci/cd, and deploying the project automatically.

## Table of contents

1. [Prerequisites](#prerequisites)
2. [Setting up ssh keys](#setting-up-ssh-keys)
3. [Gitlab ci/cd setup](#gitlab-cicd-setup)
4. [Nginx configuration](#nginx-configuration)
5. [Handling maintenance pages](#handling-maintenance-pages)
6. [Additional notes](#additional-notes)

## Prerequisites

- A **VPS** with a public ip.
- **Node.js** and **npm** installed on the vps. in this setup, we're using **node.js v22**.
- **Gitlab repository** for hosting your project.
- **PM2** for process management on your server.
- **Nginx** as the reverse proxy server.


## Setting up ssh keys

you need an ssh key to allow gitlab ci/cd to securely access your vps for deployment. follow the steps below:

### 1. Generate ssh keys on your development machine

run the following command to generate a new ssh key pair without a passphrase:

```bash
ssh-keygen -t ed25519 -C "gitlab-ci" -N "" -f ~/.ssh/id_ed25519_gitlab_ci
```

- **`-N ""`**: generates the key without a passphrase (or just press enter if you doesn't use this optionfr )
- **`-f ~/.ssh/id_ed25519_gitlab_ci`**: saves the key under a specific name to differentiate it.

### 2. Copy public key to your vps

Connect to your VPS and create a new user as "gitlab-user-deploy" using `su - gitlab-user-deploy` (or other). He needs **sudo privileges for run PM2.**

Copy the public key like this : `echo "your_public_key" >> ~/.ssh/authorized_keys` ans update file privileges : `chmod 600 ~/.ssh/authorized_keys && chmod 700 ~/.ssh`

### 3. Add the private key to gitlab ci/cd

1. copy the content of your **private key** from the file `~/.ssh/id_ed25519_gitlab_ci`.
2. go to **gitlab > settings > ci/cd > variables**.
3. add a new variable:
   - **key**: `VPS_SSH_KEY`
   - **value**: paste the content of the private key.
   - **visible**: unfortunately we can't masked whitespace characters.
   - **protect variable**: use this option
   - **expand variable**: we can use it in the gitlab-ci file

## Gitlab ci/cd setup

the ci/cd pipeline in gitlab will handle the build and deployment of your nuxt 3 application. here’s a breakdown of the `.gitlab-ci.yml` configuration:

### 1. `.gitlab-ci.yml`

```yaml
stages:
  - build
  - deploy

cache:
  paths:
    - node_modules/
    - .npm/

build:
  stage: build
  image: node:22-alpine
  before_script:
    - echo "NODE_ENV=$NODE_ENV" >> .env
    - echo "NUXT_PUBLIC_GTAG_ID=$NUXT_PUBLIC_GTAG_ID" >> .env
  script:
    - npm install --legacy-peer-deps  # install dependencies including devDependencies
    - npm run build  # build nuxt app
  artifacts:
    paths:
      - .output/  # built files
      - ecosystem.config.cjs  # pm2 config
  only:
    - main

deploy:
  stage: deploy
  image: debian
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git rsync -y )'
    - eval $(ssh-agent -s)
    - echo "$VPS_SSH_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan $VPS_HOST >> ~/.ssh/known_hosts
  script:
    - ssh $VPS_USER@$VPS_HOST "mkdir -p ~/webapp"
    - rsync -avz --ignore-errors .output/ ecosystem.config.cjs $VPS_USER@$VPS_HOST:~/webapp
    - ssh $VPS_USER@$VPS_HOST "cd ~/webapp && pm2 restart ecosystem.config.cjs"
  only:
    - main
```

### 2. Environment variables in gitlab

add the following variables in **settings > ci/cd > variables**:

- **`NODE_ENV`**: set to `production`.
- **`NUXT_PUBLIC_GTAG_ID`**: your google tag id (if used).
- **`VPS_SSH_KEY`**: your private ssh key (without passphrase).
- **`VPS_HOST`**: your VPS ip
- **`VPS_USER`**: your VPS user for deployement

### 3. `ecosystem.config.cjs` : Setup your personal runner on gitlab
### 4. `ecosystem.config.cjs` : Configuration for PM2

## Nginx configuration

Your nuxt 3 app will run behind **nginx** as a reverse proxy. here’s an example configuration for **nginx** to handle both http and https requests, along with **pm2** process management:

1. **Edit nginx configuration** file for your domain:

```bash
sudo nano /etc/nginx/sites-available/tododev.fr.conf
```

2. **Example configuration**:

```nginx
# http to https redirection
server {
    listen 80;
    listen [::]:80;
    server_name example.fr www.example.fr;
    return 301 https://$host$request_uri;
}

# https reverse proxy for node.js app
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name example.fr www.example.fr;

    #Default params for ssl settings with letsencrypt and certbot
    ssl_certificate /etc/letsencrypt/live/example.fr/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/example.fr/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

    location / {
        proxy_pass http://localhost:3000; #Same port as ecosystem.config.cjs
        proxy_http_version 2;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```

3. **Activate the site** and restart nginx:

```bash
sudo ln -s /etc/nginx/sites-available/tododev.fr.conf /etc/nginx/sites-enabled/
sudo nginx -t # verify your config
sudo systemctl restart nginx
```

## Additional notes

- **PM2** is used for managing the node.js application. ensure pm2 is installed on your vps with `npm install pm2 -g`.
- **Node.js v22** is used for this setup, so ensure your vps has the appropriate version installed. You can use any other version of node, but you'll need to update your gitlab-ci file with the right image. 
  
By following this guide, you should have a fully automated pipeline using **gitlab ci/cd** for building and deploying your **nuxt 3** application with **tailwind css** to a **vps** using **ssh** and **nginx**.