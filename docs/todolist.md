# Website Model (Showcase / E-commerce) - Todolist

## ✅ Completed

### 1. **CI/CD Setup**
   - [x] Set up GitLab CI/CD for build and deploy.
   - [x] Configured deployment pipeline using personal GitLab runner.
   - [x] Defined `ecosystem.config.cjs` for PM2 process management.

## 🔄 In Progress

### 2. **Backend Integrations**

#### 2.1 **Strapi Integration**
   - [ ] Install and configure Strapi for content management.
   - [ ] Set up API connection between Nuxt 3 and Strapi (use Axios).
   - [ ] Create content types (e.g., Blog posts, Articles, Products).
   - [ ] Implement Strapi authentication and permission management.

#### 2.2 **Prestashop Integration**
   - [ ] Configure API connection between Nuxt 3 and Prestashop.
   - [ ] Sync products and categories with Prestashop’s database.
   - [ ] Handle product variations, stock management, and orders.
   - [ ] Implement cart and checkout pages with Prestashop API.

---

## 🔜 Upcoming Tasks

### 3. **SEO & Web Performance**

#### 3.1 **SEO Optimization**
   - [ ] Implement meta tags for SEO (title, description, and keywords).
   - [ ] Add Open Graph and Twitter Card meta tags for better social sharing.
   - [ ] Include canonical URLs to avoid duplicate content.
   - [ ] Use structured data (Schema.org) for rich search results.
   - [ ] Set up a sitemap and `robots.txt`.
   - [ ] Optimize page load speed (e.g., using lazy loading for images).

#### 3.2 **Cookie Management**
   - [ ] Implement cookie consent banner (GDPR compliant).
   - [ ] Allow users to opt in/out of different cookie categories (e.g., Analytics, Marketing).
   - [ ] Store and manage user preferences.

#### 3.3 **Analytics**
   - [ ] Integrate Google Analytics (via `nuxt-gtag` module).
   - [ ] Implement event tracking for key actions (e.g., purchases, clicks).
   - [ ] Set up Facebook Pixel for retargeting.

---

### 4. **Design & Components**

#### 4.1 **Global Design Components**
   - [ ] Create reusable UI components (e.g., Buttons, Forms, Modals).
   - [ ] Implement a responsive grid system using Tailwind CSS.
   - [ ] Design and integrate the header and footer with dropdowns and navigation.
   - [ ] Add a sticky header that reappears on scroll up.
   - [ ] Add a mobile-friendly burger menu.
   - [ ] Install lottie-web

#### 4.2 **E-commerce Components**
   - [ ] Product cards and carousels.
   - [ ] Implement a dynamic shopping cart component.
   - [ ] Create a product detail page template.
   - [ ] Add a checkout form with validation (integrated with Prestashop API).

---

### 5. **Security & Compliance**

#### 5.1 **SSL & HTTPS**
   - [ ] Ensure HTTPS is enabled and redirect HTTP to HTTPS.
   - [ ] Set up SSL certificates using Let’s Encrypt (via Certbot).
   
#### 5.2 **Security Best Practices**
   - [ ] Secure the API endpoints (e.g., authentication, rate-limiting).
   - [ ] Add security headers via Nginx (e.g., `Content-Security-Policy`, `X-Content-Type-Options`).
   - [ ] Implement data encryption for sensitive data (e.g., passwords).

#### 5.3 **Privacy & GDPR**
   - [ ] Add privacy policy page.
   - [ ] Implement a process for users to manage/delete their data.
   - [ ] Handle user data securely, following GDPR regulations.

---

### 6. **Other Features**

#### 6.1 **Blog Integration**
   - [ ] Connect the blog with Strapi CMS for dynamic content.
   - [ ] Implement pagination and search for blog posts.
   - [ ] Add social sharing buttons.

#### 6.2 **Contact Forms & Lead Generation**
   - [ ] Create and style a contact form (integrated with Strapi).
   - [ ] Add spam protection (e.g., CAPTCHA or Honeypot).
   - [ ] Implement lead tracking (e.g., HubSpot, Google Tag Manager).

#### 6.3 **Static Pages**
   - [ ] Design pages like "About Us," "FAQ," and "Terms & Conditions."
   - [ ] Localize content with i18n (English and French).
   - [ ] Set up Nuxt static generation (SSG) for static content like blog posts.

#### 6.4 **User Authentication (Optional)**
   - [ ] Add login/registration with JWT or OAuth (using Strapi).
   - [ ] Implement user roles (e.g., admin, customer) for access control.

---

## 🛠 Tools & Technologies Checklist

- **Nuxt 3** (Vue.js Framework)
- **Tailwind CSS** (for styling)
- **Strapi CMS** (for content management)
- **Prestashop** (for e-commerce backend)
- **PM2** (for process management)
- **Nginx** (for web server and reverse proxy)
- **GitLab CI/CD** (for automated builds and deployments)
- **Google Analytics / Facebook Pixel** (for tracking and analytics)


### Other Interesting Features to Consider

#### 1. **Progressive Web App (PWA) Support**
   - [ ] Enable offline mode using service workers.
   - [ ] Add a web app manifest for better mobile integration.
   - [ ] Implement push notifications for key updates (e.g., new products, blog posts).

#### 2. **Performance Optimization**
   - [ ] Use **lazy loading** for images and other media to improve page load time.
   - [ ] Implement **code splitting** to reduce the JavaScript bundle size.
   - [ ] Compress images and static assets (e.g., using `nuxt/image` module).
   - [ ] Set up **CDN** (e.g., Cloudflare) to deliver assets faster globally.

#### 3. **Accessibility**
   - [ ] Ensure all interactive elements are accessible via keyboard.
   - [ ] Test color contrast and make sure all text is legible.
   - [ ] Add alt attributes to all images for better screen reader support.
   - [ ] Use ARIA attributes for enhanced accessibility.

#### 4. **Social Integration**
   - [ ] Add social login options (e.g., Google, Facebook).
   - [ ] Implement user comments on blog posts (could be integrated with Strapi).
   - [ ] Add a referral program (track referrals via Prestashop or Strapi).

#### 5. **Search Functionality**
   - [ ] Implement site-wide search (for products and blog posts).
   - [ ] Enable search filtering by categories, tags, or attributes.
   - [ ] Add real-time search suggestions.

#### 6. **User-Generated Content**
   - [ ] Allow users to submit reviews or testimonials (moderated via Strapi).
   - [ ] Implement user photo galleries or community-driven content.
   - [ ] Add a product rating system.

---

### Documentation Checklist

#### 1. **CI/CD Documentation**
   - [ ] Overview of the CI/CD pipeline and the purpose of each stage.
   - [ ] Instructions for setting up GitLab CI/CD with a runner.
     - [ ] Runner setup on the VPS (installation, registration, configuration).
     - [ ] How to assign tags and configure the runner for specific jobs.
     - [ ] Fallback to shared GitLab runners.
   - [ ] Explanation of the `.gitlab-ci.yml` structure (stages, rules, artifacts).
   - [ ] Description of environment variables used in the pipeline (e.g., `NODE_ENV`, `VPS_SSH_KEY`).

#### 2. **Backend Integrations**
   - [ ] Step-by-step guide for integrating Strapi (API configuration, content types).
   - [ ] Instructions for integrating Prestashop (API configuration, handling products).
   - [ ] How to configure authentication (if applicable).

#### 3. **SEO & Analytics**
   - [ ] Overview of the SEO strategy (meta tags, structured data, canonical URLs).
   - [ ] Setup guide for Google Analytics and Facebook Pixel.
   - [ ] Best practices for optimizing performance (image optimization, lazy loading).

#### 4. **Nginx & PM2**
   - [ ] Guide for setting up Nginx as a reverse proxy.
   - [ ] PM2 process management overview (how to start, stop, and manage apps).
   - [ ] SSL setup with Let’s Encrypt (certificates, renewal process).

#### 5. **Design Components**
   - [ ] Explanation of reusable design components (header, footer, buttons).
   - [ ] Documentation on how to add new components (tailored to both developers and designers).
   - [ ] Overview of responsive design principles applied (e.g., using Tailwind CSS).

#### 6. **Cookies & Privacy**
   - [ ] Guide for implementing a cookie consent banner.
   - [ ] Overview of how user preferences are managed and stored.
   - [ ] Documentation on GDPR compliance and data privacy measures.

#### 7. **Deployment & Maintenance**
   - [ ] Detailed guide for deploying updates (automated via GitLab CI/CD).
   - [ ] Instructions for handling maintenance mode (custom maintenance page).
   - [ ] Rollback process in case of failed deployments.