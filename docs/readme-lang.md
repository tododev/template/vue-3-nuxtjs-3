Changer la langue dans Nuxt 3 nécessite de gérer l'internationalisation (i18n). Vous pouvez le faire en utilisant le module `@nuxtjs/i18n`, qui est une solution populaire et bien intégrée dans l'écosystème Nuxt.

### Étapes pour configurer l'i18n dans Nuxt 3 :

1. **Installer le module i18n pour Nuxt :**

   Installez le module :

   Avec Bun :
   ```bash
   bun add @nuxtjs/i18n
   ```

2. **Ajouter le module dans votre configuration Nuxt :**

   Ouvrez le fichier `nuxt.config.ts` et ajoutez le module `@nuxtjs/i18n` :

   ```typescript
   export default defineNuxtConfig({
     modules: [
       '@nuxtjs/i18n'
     ],
     i18n: {
       locales: [
         { code: 'en', name: 'English', iso: 'en-US', file: 'en.json' },
         { code: 'fr', name: 'Français', iso: 'fr-FR', file: 'fr.json' }
       ],
       defaultLocale: 'en',
       langDir: 'locales/', // Répertoire où sont stockés les fichiers de traduction
       vueI18n: {
         legacy: false, // Utiliser la composition API de vue-i18n
         locale: 'en',
         fallbackLocale: 'en',
       }
     }
   })
   ```

3. **Créer les fichiers de traduction :**

   Dans votre projet, créez un dossier `locales/` où vous allez stocker les fichiers de traduction (par exemple, `en.json` et `fr.json`).

   Exemple de structure :

   ```
   my-nuxt-app/
   ├── locales/
   │   ├── en.json
   │   └── fr.json
   ```

   Exemple de contenu pour `en.json` :

   ```json
   {
     "welcome": "Welcome to our website!",
     "about": "About Us"
   }
   ```

   Exemple de contenu pour `fr.json` :

   ```json
   {
     "welcome": "Bienvenue sur notre site web!",
     "about": "À propos de nous"
   }
   ```

4. **Utiliser les traductions dans vos composants :**

   Dans vos composants Vue, vous pouvez utiliser la fonction `$t` pour accéder aux traductions :

   ```vue
   <template>
     <div>
       <h1>{{ $t('welcome') }}</h1>
       <p>{{ $t('about') }}</p>
     </div>
   </template>
   ```

   Cela affichera le texte dans la langue active.

5. **