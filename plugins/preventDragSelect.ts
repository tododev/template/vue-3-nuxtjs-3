export default defineNuxtPlugin(() => {
  if (process.client) {
    // Empêcher le drag des images
    window.addEventListener('dragstart', (e: DragEvent) => {
      const target = e.target as HTMLElement;
      if (target?.tagName === 'IMG') {
        e.preventDefault();
      }
    });

    // Empêcher la sélection de texte avec la souris (hors INPUT et TEXTAREA)
    window.addEventListener('mousedown', (e: MouseEvent) => {
      const target = e.target as HTMLElement;
      if (target?.tagName === 'INPUT' || target?.tagName === 'TEXTAREA') {
        return; // Laisser la sélection dans les champs de texte
      }
      e.preventDefault(); // Empêcher la sélection
    }, { capture: true });

    // Empêcher la sélection de texte via le clavier ou la souris
    document.addEventListener('selectstart', (e: Event) => {
      e.preventDefault();
    });
  }
});
