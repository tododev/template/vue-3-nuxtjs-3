module.exports = {
  apps: [
    {
      name: 'TodoDev vitrine',
      port: '3001',
      exec_mode: 'cluster',
      instances: 'max',
      script: './server/index.mjs'
    }
  ]
}
