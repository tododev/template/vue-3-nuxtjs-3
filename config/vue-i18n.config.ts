// config/vue-i18n.config.ts

export default {
  legacy: false, // Utilise la Composition API de vue-i18n
  locale: 'en',
  fallbackLocale: 'en',
};