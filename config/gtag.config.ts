// gtag.config.ts
// https://nuxt.com/modules/gtag

export const gtag = {
  id: process.env.NUXT_PUBLIC_GTAG_ID,
  config: {
    anonymize_ip: false, // Example setting for anonymizing IPs
    send_page_view: true, // Automatically send page view events
  },
  enabled: process.env.NODE_ENV === 'production',
  debug: process.env.NODE_ENV === 'development',
};
