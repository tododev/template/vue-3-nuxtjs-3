// i18n.config.ts
// https://i18n.nuxtjs.org/docs/getting-started/usage
import { type NuxtI18nOptions } from '@nuxtjs/i18n'
import languages from './languages.json'; // Import the languages JSON

export const i18n: NuxtI18nOptions = {
  // utiliser le fichier languages.json pour les traductions
  locales: languages,
  defaultLocale: 'en',
  lazy: false, // 
  langDir: 'locales/',
  strategy: 'no_prefix', // Ajoute ou non le préfixe de la langue dans l'URL
  vueI18n: './config/vue-i18n.config.ts',
};