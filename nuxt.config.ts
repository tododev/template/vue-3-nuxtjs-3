// nuxt.config.ts
// https://nuxt.com/docs/api/configuration/nuxt-config

import { i18n } from './config/i18n.config'
import { gtag } from './config/gtag.config'

export default defineNuxtConfig({
  compatibilityDate: '2024-04-03',
  devtools: { enabled: true },
  ssr: true,
  modules: [
    '@nuxtjs/tailwindcss',
    '@nuxtjs/i18n',
    'nuxt-gtag',
    ['@nuxtjs/google-fonts', {
      families: {
        Montserrat: true,
      },
      display: 'swap',
      download: true,
      base64: true,
      inject: true,
      fontsDir: 'assets/fonts',
    }],
    '@nuxtjs/seo',
  ],
  plugins: [
    { src: '~/plugins/preventDragSelect.ts', mode: 'client' },
    { src: '~/plugins/vue3-toastify.ts', mode: 'client' }
  ],
  css: ["~/assets/css/main.css"],
  i18n,
  gtag,
  site: {
    // https://nuxtseo.com/nuxt-seo/guides/configuring-modules
    url: 'https://example.com',
    name: 'Awesome Site',
    description: 'Welcome to my awesome site!',
    defaultLocale: 'en', // not needed if you have @nuxtjs/i18n installed
  }
})